package circleAndPoints;

import java.util.ArrayList;

public class PointList {

    private ArrayList<Point> pointList = new ArrayList<>();

    public void add(Point point) {
        this.pointList.add(point);
    }

    public Point[] toArray() {
        return this.pointList.toArray(new Point[0]);
    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder();

        for (Point p : this.pointList) {
            result.append(p.toString()).append("\n");
        }

        return result.toString();

    }

}
